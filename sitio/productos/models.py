from django.db import models

class Marca(models.Model):
    nombre = models.CharField('Nombre de la Marca', max_length=200)
    def __str__(self):
        return self.nombre

class Modelo(models.Model):
    foto = models.ImageField('Foto', upload_to='images/')
    nombre = models.CharField('Nombre del Modelo', max_length=200)
    marca = models.ForeignKey(Marca, on_delete=models.CASCADE)
    descripcion = models.CharField('Descripción', max_length=500)
    precio = models.IntegerField('Precios en soles', default=0)
    unidades = models.IntegerField('Cantidad en unidades', default=0)
    fecha_pub = models.DateTimeField('Fecha de publicación')

    def __str__(self):
        return self.nombre