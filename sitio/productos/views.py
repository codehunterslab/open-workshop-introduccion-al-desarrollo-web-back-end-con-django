from django.shortcuts import render
from django.http import Http404
from .models import Modelo

def inicio(request):
    lista_modelos = Modelo.objects.all()
    context = {'lista_modelos': lista_modelos}
    return render(request, 'productos/inicio.html', context)

def detalle(request, modelo_id):
	try:
		modelo = Modelo.objects.get(pk=modelo_id)
	except Modelo.DoesNotExist:
		raise Http404("Este modelo no existe")
	return render(request, 'productos/detalle.html', {'modelo': modelo})