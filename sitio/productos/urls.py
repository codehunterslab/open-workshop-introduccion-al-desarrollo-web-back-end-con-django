from django.urls import path
from . import views

urlpatterns = [
	path('',views.inicio,name='inicio'),
	path('<int:modelo_id>/',views.detalle,name='detalle'),
]